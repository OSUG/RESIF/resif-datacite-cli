Résif DataCite CLI
==================

**resif-datacite-cli** is a command line tool to regiser/update DOI on DataCite.

More details about Résif are available on http://seismology.resif.fr/

Installation
------------

From sources
^^^^^^^^^^^^

**resif-datacite-cli** sources are distributed under the terms of `GPLv3 licence <https://choosealicense.com/licenses/gpl-3.0>`_
and are available at https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/resif-datacite-cli

First, ensure Git and Python 3 are installed on your system.

.. code-block:: bash

    $ sudo apt install git
    $ sudo apt install python3 python3-virtualenv python3-pip python3-setuptools python3-wheel

Then, clone the project from the Gitlab repository:

.. code-block:: bash

    $ git clone https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/RESIF/resif-datacite-cli.git
    $ cd resif-doi-cli

Then, create and activate a virtualenv:

.. code-block:: bash

    $ python3 -m venv venv
    $ source venv/bin/activate

Finally, install the project and its dependencies:

.. code-block:: bash

    $ pip install --upgrade pip setuptools wheel
    $ pip install -e .

Configuration
-------------

Create a file '.env' to set the following environment variables:

.. code-block::

    # Your DataCite account
    DATACITE_PREFIX=
    DATACITE_LOGIN=
    DATACITE_PASSWORD=

    # Use the DataCite test infrastructure
    DATACITE_TEST=True

    # The DataCite schema version to use
    DATACITE_SCHEMA_VERSION=4.4

    # Base URL to use to build a landing page URL
    DATACITE_BASE_URL=http://seismology.resif.fr

.. note::

    You will find a skeleton inside the **.env-dist** file. Fill and uncomment the necessary lines.

Usage
-----

General syntax:

.. code-block:: bash

    $ resif-datacite-cli [OPTIONS] COMMAND [ARGS]

Use the **--help** option to show the specific syntax of each command.

Available commands
------------------
.. code-block:: bash

    $ resif-datacite-cli --help
    Usage: resif-datacite-cli [OPTIONS] COMMAND [ARGS]...

    Options:
      --help  Show this message and exit.

    Commands:
      download  Download a DOI XML file from DataCite
      upload    Register/Update a DOI on DataCite
      validate  Validate a DataCite XML file

Validate
^^^^^^^^

.. code-block:: bash

    $ resif-datacite-cli validate --help
    Usage: resif-datacite-cli validate [OPTIONS] PATH

      Validate a DataCite XML file

    Options:
      -v, --verbosity LVL  Either CRITICAL, ERROR, WARNING, INFO or DEBUG
      --prefix TEXT        DataCite account prefix
      --login TEXT         DataCite account login
      --password TEXT      DataCite account password
      --test               Use the DataCite TEST platform
      --schema TEXT        DataCite schema version
      --baseurl TEXT       Base URL for landing pages
      --stop / --no-stop   Stop execution on first error
      --help               Show this message and exit.

Upload
^^^^^^

.. code-block:: bash

    $ resif-datacite-cli upload --help
    Usage: resif-datacite-cli upload [OPTIONS] PATH

      Register/Update a DOI on DataCite

    Options:
      -v, --verbosity LVL  Either CRITICAL, ERROR, WARNING, INFO or DEBUG
      --prefix TEXT        DataCite account prefix
      --login TEXT         DataCite account login
      --password TEXT      DataCite account password
      --test               Use the DataCite TEST platform
      --schema TEXT        DataCite schema version
      --baseurl TEXT       Base URL for landing pages
      --stop / --no-stop   Stop execution on first error
      --help               Show this message and exit.

Download
^^^^^^^^

.. code-block:: bash

    $ resif-datacite-cli download --help
    Usage: resif-datacite-cli download [OPTIONS] SUFFIX OUTPUT_DIR

      Download a DOI XML file from DataCite

    Options:
      -v, --verbosity LVL  Either CRITICAL, ERROR, WARNING, INFO or DEBUG
      --prefix TEXT        DataCite account prefix
      --login TEXT         DataCite account login
      --password TEXT      DataCite account password
      --test               Use the DataCite TEST platform
      --schema TEXT        DataCite schema version
      --baseurl TEXT       Base URL for landing pages
      --help               Show this message and exit.

DataCite documentation
----------------------
Schemas
^^^^^^^

* 4.4 : https://schema.datacite.org/meta/kernel-4.4/
* 4.3 : https://schema.datacite.org/meta/kernel-4.3/
* 4.2 : https://schema.datacite.org/meta/kernel-4.2/
* 4.1 : https://schema.datacite.org/meta/kernel-4.1/
* 4.0 : https://schema.datacite.org/meta/kernel-4.0/
* 3.1 : https://schema.datacite.org/meta/kernel-3.1/
* 3.0 : https://schema.datacite.org/meta/kernel-3.0/

API Client
^^^^^^^^^^

* https://datacite.readthedocs.io/en/latest/
* https://pypi.org/project/datacite/
* https://github.com/inveniosoftware/datacite
