-i https://pypi.org/simple
attrs==22.2.0; python_version >= '3.6'
certifi==2022.12.7; python_version >= '3.6'
charset-normalizer==3.0.1
click==8.1.3
click-log==0.4.0
datacite==1.1.2
idna==3.4; python_version >= '3.5'
idutils==1.2.0; python_version >= '3.7'
isbnlib==3.10.13
jsonschema==4.17.3; python_version >= '3.7'
lxml==4.9.2
pyrsistent==0.19.3; python_version >= '3.7'
python-dotenv==0.21.1
requests==2.28.2; python_version >= '3.7' and python_version < '4'
six==1.16.0; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2'
urllib3==1.26.14; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3, 3.4, 3.5'
