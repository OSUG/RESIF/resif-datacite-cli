# coding: utf-8
from ..errors import Error


class ReaderError(Error):
    """
    Base class for errors raised by the readers
    """
    pass


class ReaderFactoryError(ReaderError):
    """
    Factory error
    """
    pass


class ReaderErrorDocumentInvalid(ReaderError):
    """
    Invalid document
    """
    pass
