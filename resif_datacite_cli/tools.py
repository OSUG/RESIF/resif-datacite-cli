# coding: utf-8

def str2bool(v):
    """
    Evaluate a string value as a boolean
    :param v: The string value
    :return: The evaluated boolean value
    :rtype: bool
    """

    if v is None:
        return False
    elif type(v) == str:
        return v.lower() in ("yes", "true", "y", "t", "1", "on")
    else:
        return v
