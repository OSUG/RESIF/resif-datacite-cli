# coding: utf-8
import os
import logging
from datacite import DataCiteMDSClient
from datacite.errors import DataCiteNotFoundError, DataCiteRequestError, DataCiteServerError, HttpError
from .models.factory import DoiFactory
from .models.errors import DoiErrorIdentifierInvalid
from .readers.factory import ReaderFactory
from .readers.errors import ReaderErrorDocumentInvalid, ReaderFactoryError
from .tools import str2bool

# Configure logger
logger = logging.getLogger()


class ServiceDoi(object):

    def __init__(self, prefix, login, password, test=True, schema_version=4.4, base_url=None):
        logger.debug("ServiceDoi(%s, %s, %s, %s, %s, %s)" % (prefix, login, password, test, schema_version, base_url))

        self.prefix = str(prefix)
        self.test = str2bool(test)
        self.base_url = base_url
        self.schema_version = schema_version

        # Initialize DataCite API client
        if login and password:
            if self.test:
                api_url = 'https://mds.test.datacite.org'
                logger.info('Using DataCite TEST server')
            else:
                api_url = 'https://mds.datacite.org'
                logger.info('Using DataCite PRODUCTION server')

            self.client = DataCiteMDSClient(
                username=login,
                password=password,
                prefix=self.prefix,
                url=api_url
            )
            logger.debug("API client initialized")
        else:
            logger.debug("API client NOT initialized")

        # Initialize the file reader
        self._reader = ReaderFactory.factory(self.schema_version)

    # DOI Validation
    ####################################################################################################################

    def validate(self, path, stop_on_error=False, offline=False):
        """
        Validate DOI from a file or a folder

        :param path: The path to validate
        :param stop_on_error: Stop the execution on first error
        :param offline: Run only the offline validation steps
        :rtype: bool
        """
        logger.debug("ServiceDoi.validate(%s, %s, %s)" % (path, stop_on_error, offline))

        # FOLDER
        if os.path.isdir(path) and not path.endswith('__MACOSX'):
            success_all = True
            for path_item in sorted(os.listdir(path), key=os.path.abspath):
                success_step = self.validate(os.path.join(path, path_item), stop_on_error, offline)
                success_all = success_all and success_step
                if stop_on_error and not success_all:
                    return success_all
            return success_all

        # FILE
        elif os.path.isfile(path) and (path.endswith('.xml') or path.endswith('.txt')):
            logger.info("Checking '%s'..." % path)

            # Offline validation steps
            ###########################

            # Load the file and check the structure
            try:
                doi = DoiFactory.factory(self._reader, path)
                logger.success("The XML structure is valid")
            except ReaderErrorDocumentInvalid as e:
                logger.error("The XML structure is invalid")
                logger.error(e)
                logger.warning("The file '%s' should NOT be uploaded!" % path)
                return False
            except ReaderFactoryError as e:
                logger.error("Unable to read file")
                logger.debug(e)
                return False

            # Check the identifier
            try:
                doi.validate()
                logger.success('The DOI is valid')
            except DoiErrorIdentifierInvalid as e:
                logger.error(e)
                logger.warning("The file '%s' should NOT be uploaded!" % path)
                return False

            logger.success("The file '%s' is ready to upload!" % path)

            # Online validation steps
            ###########################
            if not offline:

                # Check if identifier is already registered
                try:
                    location = self.client.doi_get(doi.identifier)
                    logger.success("The DOI is already registered on DataCite")

                    # Check the landing page
                    logger.debug('Registered landing page: %s' % location)
                    if location == doi.location(self.base_url):
                        logger.success("The registered landing page URL is valid")
                    else:
                        logger.debug('Generated landing page: %s' % doi.location(self.base_url))
                        logger.warning('The registered landing page URL is invalid')
                except DataCiteNotFoundError as e:
                    logger.warning("The DOI '%s' is not registered yet on DataCite" % doi.identifier)
                except DataCiteRequestError as e:
                    logger.error('Datacite Error: %s' % e)
                except DataCiteServerError as e:
                    logger.error('Datacite Server Error')
                    logger.debug(e)
                except HttpError as e:
                    logger.error('Datacite Connection Error')
                    logger.debug(e)

                # Check the metadata
                try:
                    xml_metadata = self.client.metadata_get(doi.identifier)
                    logger.success("The DOI metadata are available on DataCite")
                    logger.debug('Metadata: %s' % xml_metadata)
                except DataCiteNotFoundError as e:
                    logger.warning("No metadata found for DOI '%s' on DataCite" % doi.identifier)
                except DataCiteRequestError as e:
                    logger.error('Datacite Error: %s' % e)
                except DataCiteServerError as e:
                    logger.error('Datacite Server Error')
                    logger.debug(e)
                except HttpError as e:
                    logger.error('Datacite Connection Error')
                    logger.debug(e)

            # All is fine
            return True

        # OTHER
        else:
            logger.debug("Skipping '%s'" % path)
            return True

    # DOI Submission
    ####################################################################################################################

    def upload(self, path, stop_on_error=False):
        """
        Register DOI from a file or a folder

        :param path: The path to read
        :param stop_on_error: Stop the execution on first error
        :rtype: bool
        """
        logger.debug("ServiceDoi.upload(%s)" % path)

        # FOLDER
        if os.path.isdir(path) and not path.endswith('__MACOSX'):
            success_all = True
            for path_item in sorted(os.listdir(path), key=os.path.abspath):
                success_step = self.upload(os.path.join(path, path_item), stop_on_error)
                success_all = success_all and success_step
                if stop_on_error and not success_all:
                    return success_all
            return success_all

        # FILE
        elif os.path.isfile(path) and (path.endswith('.xml') or path.endswith('.txt')):
            logger.info("Checking '%s'..." % path)

            # Load the file and check the structure
            try:
                doi = DoiFactory.factory(self._reader, path)
                logger.success("The XML structure is valid")
            except ReaderErrorDocumentInvalid as e:
                logger.error("The XML structure is invalid")
                logger.error(e)
                logger.warning("The file '%s' has NOT been uploaded on DataCite" % path)
                return False
            except ReaderFactoryError as e:
                logger.error("Unable to read the file '%s'" % path)
                logger.debug(e)
                logger.warning("The file '%s' has NOT been uploaded on DataCite" % path)
                return False

            # Check the identifier
            try:
                doi.validate()
                logger.success('The DOI is valid')
            except DoiErrorIdentifierInvalid as e:
                logger.error(e)
                logger.warning("The file '%s' has NOT been uploaded on DataCite" % path)
                return False

            logger.success("The file '%s' is ready to upload!" % path)
            logger.info("Uploading '%s'..." % path)

            # Submit to DataCite
            try:
                # Publish metadata for the DOI
                identifier = doi.identifier
                logger.debug("Pushing the XML metadata file for '%s' on DataCite" % identifier)
                self.client.metadata_post(doi.content)
                logger.success("The file '%s' has been pushed to DataCite" % path)

                # Register/Update the DOI
                location = doi.location(self.base_url)
                logger.debug("Registering the DOI '%s' on DataCite with landing page URL: %s" % (identifier, location))
                self.client.doi_post(identifier, location)
                logger.success("The DOI '%s' has been registered on DataCite and linked to '%s'" % (identifier, location))

            except DataCiteRequestError as e:
                logger.error('Datacite Error: %s' % e)
                return False
            except DataCiteServerError as e:
                logger.error('Datacite Server Error')
                logger.debug(e)
                return False
            except HttpError as e:
                logger.error('Datacite Connection Error')
                logger.debug(e)
                return False

            # All is fine
            logger.success("Done!")
            return True

        # OTHER
        else:
            logger.debug("Skipping '%s'" % path)
            return True

    # DOI Retrieval
    ####################################################################################################################

    def download(self, suffix, output_path):
        """
        Download XML metadata of a DOI from DataCite

        :param suffix: The DOI suffix
        :param output_path: The output path/directory
        :rtype: bool
        """

        # Build the identifier
        identifier = '%s/%s' % (self.prefix, suffix)
        logger.info("Downloading DOI '%s' from DataCite..." % identifier)

        # Check if identifier is already registered
        try:
            location = self.client.doi_get(identifier)
            logger.success("The landing page of DOI '%s' registered on DataCite is '%s'" % (identifier, location))
        except DataCiteNotFoundError as e:
            logger.warning("The DOI '%s' is not registered on DataCite" % identifier)
            return False
        except DataCiteRequestError as e:
            logger.error('Datacite Error: %s' % e)
            return False
        except DataCiteServerError as e:
            logger.error('Datacite Server Error')
            logger.debug(e)
            return False
        except HttpError as e:
            logger.error('Datacite Connection Error')
            logger.debug(e)
            return False

        # Check the metadata
        try:
            xml_metadata = self.client.metadata_get(identifier)
            logger.debug('Metadata: %s' % xml_metadata)

            if os.path.isdir(output_path):
                dir_path = os.path.join(output_path, self.prefix)

                if '/' in suffix:
                    tokens = suffix.split('/')
                    dir_path = os.path.join(dir_path, *tokens[:-1])
                    file_name = '%s.xml' % tokens[-1]
                else:
                    file_name = '%s.xml' % suffix

                os.makedirs(dir_path, exist_ok=True)
                output_path = os.path.join(dir_path, file_name)

            logger.debug("Output path: %s" % output_path)
            with open(output_path, 'w') as f:
                f.write(xml_metadata)

            logger.success("The metadata of DOI '%s' available on DataCite have been downloaded as '%s'" % (identifier, output_path))

        except DataCiteNotFoundError as e:
            logger.warning("No metadata found for DOI '%s' on DataCite" % identifier)
            return False
        except DataCiteRequestError as e:
            logger.error('Datacite Error: %s' % e)
            return False
        except DataCiteServerError as e:
            logger.error('Datacite Server Error')
            logger.debug(e)
            return False
        except HttpError as e:
            logger.error('Datacite Connection Error')
            logger.debug(e)
            return False

        # All is fine
        return True
