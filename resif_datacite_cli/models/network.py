# coding: utf-8
import re
from . import DoiAbstract

# Regular expression for DOI identifier
regex_identifier = re.compile('^10\.15778\/RESIF\.((?:[A-W][0-9A-Z])|(?:[XYZ0-9][0-9A-Z][0-9]{4}))$', flags=re.IGNORECASE)
regex_temp = re.compile("^10\.15778\/RESIF\.([XYZ0-9][A-Z0-9])([0-9]{4})$", flags=re.IGNORECASE)


class DoiResifNetwork(DoiAbstract):
    """
    DOI content structure for a RESIF network
    """

    # Constructor
    def __init__(self, identifier, content):
        super(DoiResifNetwork, self).__init__(identifier, content)

    # Validation
    ####################################################################################################################

    def _validate_identifier(self):
        """
        Validate the identifier

        :rtype: bool
        """
        if not regex_identifier.match(self.identifier):
            return False

        return True

    # Identifier
    ####################################################################################################################

    @property
    def extended_code(self):
        """
        Return the extended code of the network

        :rtype: str
        """
        #print("Extended code: %s" % regex_identifier.search(self.identifier).group(1))
        return regex_identifier.search(self.identifier).group(1)

    @property
    def code(self):
        """
        Return the code year if the network is temporary

        :rtype: str
        """
        if self.is_temporary:
            #print("Code: %s" % regex_temp.search(self.identifier).group(1))
            return regex_temp.search(self.identifier).group(1)
        else:
            return self.extended_code

    @property
    def year_start(self):
        """
        Return the start year if the network is temporary

        :rtype: int
        """
        if self.is_temporary:
            #print("Code: %s" % regex_temp.search(self.identifier).group(2))
            return regex_temp.search(self.identifier).group(2)
        else:
            return None

    @property
    def is_temporary(self):
        """
        Return True if the network is temporary

        :rtype: bool
        """
        if regex_temp.match(self.identifier):
            #print("Est temporaire: %s" % self.identifier)
            return True
        else:
            #print("Est permanent: %s" % self.identifier)
            return False

    # Landing page
    ####################################################################################################################

    # Old portal:
    #############
    # Perm: http://seismology.resif.fr/#NetworkConsultPlace:FR
    # TEmp: http://seismology.resif.fr/#NetworkConsultPlace:1A%5B2009-01-01T00:00:00_2012-12-31T23:59:59%5D

    # New portal
    ############
    # Perm: https://www7.obs-mip.fr/resif/networks/#/FR
    # Temp: https://www7.obs-mip.fr/resif/networks/#/1A__2009

    def location(self, base_url='https://seismology.resif.fr'):
        """
        Build the landing page URL from a base URL (according to the new portal)

        :param base_url: The base URL to use (default='http://seismology.resif.fr')
        :return: The landing page URL
        :rtype: str
        """

        # Remove the trailing slash of base URL
        if base_url.endswith('/'):
            base_url = base_url.rstrip('/')

        # Build the location URL
        if self.is_temporary:
            #print('%s/networks/#/%s__%s' % (base_url, self.code, self.year_start))
            return '%s/networks/#/%s__%s' % (base_url, self.code, self.year_start)
        else:
            #print('%s/networks/#/%s' % (base_url, self.extended_code))
            return '%s/networks/#/%s' % (base_url, self.extended_code)
